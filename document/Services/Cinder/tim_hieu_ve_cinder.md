# Openstack Block Storage (Cinder)


# MỤC LỤC
- [1.Khái niệm về Cinder](#cinder1)
- [2.Cinder architecture](#cinder2)
- [3.Cinder workflow](#cinder3)
	- [3.1.Volume Creation Workflow](#cinder3.1)
	- [3.2.Volume Attach Workflow](#cinder3.2)
- [4.Cinder status](#cinder4)
- [5.Tài liệu tham khảo](#cinder5)

<a name="cinder1"></a>

# 1.Khái niệm về Cinder
\- Cinder là 1 Block Storage service cung cấp thiết bị lưu trữ theo kiểu persistent block cho instances.  
\- Cung cấp và quản lý các tài nguyên lưu trữ dạng persistent block storage (volume) cho các máy ảo.

\- Các volume này có thể được tách từ máy ảo này và gán lại vào một máy ảo khác, mà dữ liệu được giữ nguyên không bị thay đổi.

\- Hiện tại, một volume chỉ có thể được gán (attached) và một máy ảo tại một thời điểm.

\- Các volume tồn tại độc lập với các máy ảo (tức là khi máy ảo bị xóa thì volume không bị xóa).

\- Phân chia tài nguyên lưu trữ thành các khối gọi là Cinder volume.

\- Cung cấp các API như là tạo, xóa, backup, restore, tạo snapshot, clone volume và nhiều hơn nữa. Những API này thực hiện bởi các backend lưu trữ mà được cinder hỗ trợ.


<a name="cinder2"></a>

# 2.Cinder architecture

![image](/uploads/f0175a9ff081f2c9a9bb0bf0c987b13d/image.png)

\- `cinder-api`: Chấp nhận các yêu cầu API và định tuyến chúng đến `cinder-volume` để thực hiện.  
\- `cinder-scheduler`: Lịch trình và đường đi cho các requests đến volume service thích hợp, thành phần tương tự với  `nova-scheduler`  
\- `cinder-volume`: Tương tác trực tiếp với Block Storage service và các processe như `cinder-scheduler`. Nó cũng tương tác với các quá trình này thông qua message queue.  `Cinder-volume` phản hồi các yêu cầu đọc và ghi được gửi đến Block Storage service để duy trì trạng thái. Nó có thể tương tác với nhiều nhà cung cấp lưu trữ thông qua driver architecture
\- cinder-backup: Cung cấp phương pháp để backup 1 volume đến nhà cung cấp dịch vụ lưu trữ backup  
\- Messaging queue: Thông tin định tuyến giữa Block Storage processes.
>Note:  
>\- Ngoài ta, AMQP (Advanced message queue protocol) là giao thức phổ biến mà RabbitMQ dùng (RabbitMQ hỗ trợ nhiều giao thức).  
>\- RabbitMQ sẽ nhận message đến từ các thành phần khác nhau trong hệ thống, lưu trữ chúng an toàn trước khi đẩy đến đích.  


<a name="cinder3"></a>

# 3.Cinder workflow

<a name="cinder3.1"></a>

## 3.1.Volume Creation Workflow
\- Quá trình tạo 1 volume:  

![image](/uploads/a150330e16859e7cae067986f959b031/image.png)

1.Client yêu cầu tạo volume thông qua REST API của cinder ( client sử dụng python-cinderclient hoặc thông qua dashboard)  
2.cinder-api xác thực yêu cầu hợp lệ ko?, thông tin user. Mội khi được xác thực, put message lên AMQP queue để xử lý.  
3.cinder-volume xử lý message của queue, gửi message cho cinder-scheduler để xác định backend cung cấp cho volume.  
4.cinder-scheduler xử lý message của queue, sinh ra danh sách các ứng viên (node storage) dựa trên trạng thái hiện tại và yêu cầu tiêu chí về volume criteria (size, availability zone, volume type (including extra specs)).  
5.cinder-volume đọc thông tin trả lời cinder-scheduler từ queue, lặp lại danh sách ứng viên bằng phương thức backend driver cho đến khi thành công.  
6.Cinder driver tạo volume được yêu cầu thông qua tương tác với hệ thống storage ( phụ thuộc vào cấu hình và giao thức)  
7.cinder-volume tập hợp volume metadata từ queue và kết nối thông tin và chuyển message trả lời đến AMQP queue.  
8.cinder-api đọc message trả lời từ queue và trả lời client.  
9.Client nhận được thông tin bao gồm trạng thái của yêu cầu tạo volume: volume UUID, etc.  


<a name="cinder3.2"></a>

## 3.2.Volume Attach Workflow  

![image](/uploads/0308a4cbb80b536137928aafec45065f/image.png)

1.Client yêu cầu attach volume thông Nova Rest API ( client sử dụng python-cinderclient hoặc thông qua dashboard)  
2.nova-api xác thực yêu cầu xem có hợp lệ hay không? , thông tin user. Một khi được xác thực, gọi Cinder API để lấy thông tin về volume cụ thể.  
3.cinder-api xác thực yêu cầu xem có hợp lệ hay không?, thông tin user. Một khi được xác thực, post message đến volume manager thông qua AMQP.  
4.cinder-volume đọc message từ queue, gọi Cinder driver tương ứng đến volume để attached.  
5.Cinder driver chuẩn bị Cinder volume cho việc attachment ( các bước cụ thể phụ thuộc vào giao thức storage được sử dụng)  
6.cinder-volume post thông tin trả lời đến cinder-api thông qua AMQP queue.  
7.cinder-api đọc message trả lời từ cinder-volume từ queue, truyền thông tin kết nối trong RESTful reponse đến Nova caller.  
8.Nova tạo kết nối đến storage với thông tin trả lại từ Cinder.  
9.Nova truyền volume device/file đến hypervisor, sau đó attach volume device/file đến guest VM như một thiết bị block thực tế hoặc (phụ thuộc vào giao thức storage).  

<a name="cinder4"></a>

# 4.Cinder status

|Status|Mô tả|
|---|---|
|Creating|Volume được tạo ra|
|Available|Volume ở trạng thái sẵn sàng để attach vào một instane|
|Attaching|Volume đang được gắn vào một instane|
|In-use|Volume đã được gắn thành công vào instane|
|Deleting|Volume đã được xóa thành công|
|Error|Đã xảy ra lỗi khi tạo Volume|
|Error deleting|Xảy ra lỗi khi xóa Volume|
|Backing-up|Volume đang được back up|
|Restore_backup|Trạng thái đang restore lại trạng thái khi back up|
|Error_restoring|Có lỗi xảy ra trong quá trình restore|
|Error_extending|Có lỗi xảy ra khi mở rộng Volume|


<a name="cinder5"></a>
### Tài liệu tham khảo 

[1] https://docs.openstack.org//cinder/latest/doc-cinder.pdf  
[2] http://netapp.github.io/openstack-deploy-ops-guide/icehouse/content/section_cinder-processes.html






 	 	
