# Openstack Network Service (Neutron)


# MỤC LỤC
- [1.Khái niệm về Neutron](#neutron1)
- [2.Neutron architecture](#neutron2)

<a name="neutron1"></a>

# 1.Khái niệm về Neutron
- Neutron xử lý việc tạo và quản lý cơ sở hạ tầng mạng ảo trong OpenStack cloud. Các yếu tố cơ sở hạ tầng bao gồm mạng, mạng con và routers. Chúng ta cũng có thể triển khai các dịch vụ nâng cao như tường lửa hoặc mạng riêng ảo (VPN)   

- Neutron cung cấp cho nhà quản trị sự linh hoạt để quyết định dịch vụ riêng lẻ nào sẽ chạy trên hệ thống vật lý nào. Tất cả các trình nền dịch vụ có thể được chạy trên một máy chủ vật lý duy nhất cho các mục đích đánh giá. Ngoài ra, mỗi dịch vụ có thể có một máy chủ vật lý duy nhất hoặc được sao chép trên nhiều máy chủ để cung cấp dự phòng.


<a name="neutron2"></a>

# 2.Neutron architecture

![image](/uploads/bd0956c2dada89e6c6a57f178e39665e/image.png)  


- Neutron server: Chức năng của thành phần này là  của toàn bộ môi trường Neutron ra thế giới bên ngoài. Về cơ bản, nó được tạo thành từ ba mô-đun:  
   -   REST service: Dịch vụ REST chấp nhận các yêu cầu API từ các thành phần khác và hiển thị tất cả các chi tiết hoạt động nội bộ về mạng, mạng con, cổng, v.v. Nó là một ứng dụng WSGI được viết bằng Python và sử dụng cổng 9696 để giao tiếp.  
   -   RPC service: Dịch vụ RPC giao tiếp với messaging bus và cho phép giao tiếp hai chiều giữa các agent.  
   -   Plugin: Các plugin này được chia thành các plugin cốt lõi, triển khai API Neutron cốt lõi, là mạng lớp 2 (chuyển mạch) và quản lý địa chỉ IP. Nếu bất kỳ plugin nào cung cấp các dịch vụ mạng bổ sung, chúng tôi gọi đó là plugin dịch vụ - ví dụ: Cân bằng tải như một dịch vụ (LBaaS), Tường lửa dưới dạng dịch vụ (FWaaS), v.v   

- L2 agent: L2 agent chạy trên hypervisor (node computer) và chức năng của nó là kết nối các thiết bị mới,nghĩa là nó cung cấp kết nối đến các máy chủ mới trong các phân đoạn mạng thích hợp và cũng cung cấp thông báo khi thiết bị được gắn hoặc gỡ bỏ  

- L3 agent: L3 agent chạy trên nút mạng và chịu trách nhiệm định tuyến tĩnh, chuyển tiếp IP và các tính năng khác của L3, chẳng hạn như DHCP





 	 	
