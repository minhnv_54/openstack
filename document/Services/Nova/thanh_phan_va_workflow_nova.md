# Tìm hiểu về Nova ( Openstack compute service)
# Mục lục
<h3><a href="#overview_nova">1. Giới thiệu Nova</a></h3>
<h3><a href="#services_nova">2. Compute Services</a></h3>
<h3><a href="#workflow_nova">3. Workflow Nova</a></h3>
<h3><a href="#ref_nova">4. Tham khảo</a></h3>
---

<h2><a name="overview_nova">1. Giới thiệu Nova</a></h2>
<div>
<ul>
<li>OpenStack Compute đóng vai trò là cốt lõi của OpenStack cloud, cung cấp và quản lý các máy ảo theo yêu cầu: khởi tạo, lập lịch, ngừng hoạt động,...</li>
<li>Nova bao gồm nhiều tiến trình trên server, mỗi tiến trình thực hiện các chức năng khác nhau. Nova cung cấp <b>REST API</b> để tương tác với ứng dụng client phía người dùng, các thành phần bên trong Nova giao tiếp thông qua <b>RPC</b></li>
<li>Các API servers xử lý các REST request, gửi các bản tin RPC tới các microservice khác của Nova nếu cần. Các bản tin RPC dược thực hiện nhờ thư viện  <b>oslo.messaging</b> - lớp trừu tượng ở phía trên của các message queue. Hầu hết các thành phần của nova có thể chạy trên nhiều server và có một trình quản lý lắng nghe các bản tin RPC. Ngoại trừ <b>nova-compute</b>, vì dịch vụ <b>nova-compute</b> được cài đặt trên các máy compute - các máy cài đặt hypervisor mà <b>nova-compute</b> quản lý.</li>
<li>Nova cũng sử dụng một cơ sở dữ liệu trung tâm chia sẻ chung giữa các thành phần. Tuy nhiên, vì mục tiêu nâng cấp, các cơ sở dữ liệu được truy cập thông qua một lớp đối tượng dể đảm bảo các thành phần kiểm soát đã nâng cấp vẫn có thể giao tiếp với nova-compute ở phiên bản trước đó. Để thực hiện điều này, nova-compute ủy nhiệm các yêu cầu tới cơ sở dữ liệu thông qua RPC tới một trình quản lý trung tâm, chính là dịch vụ <b>nova-conductor</b>.</li> 
</ul>
</div>
![image](/uploads/9f5e97259eedf231c0b104c3793fc3c0/image.png)

<h2><a name="services_nova">2. Compute Service</a></h2>
<br>
![image](/uploads/65a552c7085171082908e6a84f1d72b4/image.png)
<br> 
Computes node control các máy chủ ảo và một cloud node control chứa các compute service còn lại
<br> 
Các thành phần của Nova: gồm 5 thành chính: nova-api, nova-scheduler, nova-conductor, nova-compute, SQL database
<ul>
<li><b>nova-api </b>Tiếp nhận và phản hồi các lời gọi API từ người dùng cuối. Dịch vụ này hỗ trợ OpenStack Compute API, Amazon EC2 API.
<li><b>nova-compute </b>Một worker daemon thực hiện tác vụ quản lý vòng đời các máy ảo như: tạo và hủy các instance thông qua các hypervisor APIs. Ví dụ:
<ul>
<li>XenAPI đối với XenServer/XCP</li>
<li>libvirt đối với KVM hoặc QEMU</li>
<li>VMwareAPI đối với VMware</li>
</ul>
<li><b>nova-scheduler </b>Daemon này lấy các yêu cầu tạo máy ảo từ hàng đợi và xác định xem server compute nào sẽ được chọn để vận hành máy ảo.</li>
<li><b>nova-conductor </b>Là module trung gian tương tác giữa <b>nova-compute</b> và cơ sở dữ liệu. Nó hủy tất cả các truy cập trực tiếp vào cơ sở dữ liệu tạo ra bởi <b>nova-compute</b> nhằm mục đích bảo mật, tránh trường hợp máy ảo bị xóa mà không có chủ ý của người dùng.</li>
<li><b>nova-cert </b>Là một worker daemon phục vụ dịch vụ Nova Cert cho chứng chỉ X509, được sử dụng để tạo các chứng chỉ cho <b>euca-bundle-image</b>. Dịch vụ này chỉ cần thiết khi sử dụng EC2 API.</li>
<li><b>nova-consoleauth </b>Ủy quyền tokens cho người dùng mà console proxies cung cấp. Dịch vụ này phải chạy với console proxies để làm việc.</li>
<li><b>nova-xvpvncproxy </b>Cung cấp một proxy truy cập máy ảo đang chạy thông qua kết nối VNC.</li>
<li><b>nova client </b>Cho phép người dùng thực hiện tác vụ quản trị hoặc các tác vụ thông thường của người dùng cuối.</li>
<li><b>The queue </b>Là một trung tâm chuyển giao bản tin giữa các daemon. Thông thường queue này cung cấp bởi một phần mềm message queue hỗ trợ giao thức AMQP: RabbitMQ, Zero MQ.</li>
<li><b>SQL database </b>Lưu trữ hầu hết trạng thái ở thời điểm biên dịch và thời điểm chạy cho hạ tầng cloud:
<ul>
<li>Các loại máy ảo đang có sẵn</li>
<li>Các máy tính đang đưa vào sử dụng</li>
<li>Hệ thống mạng sẵn sàng</li>
<li>Các projects.</li>
</ul>
Về cơ bản, OpenStack Compute hỗ trợ bất kỳ hệ quản trị cơ sở dữ liệu nào như SQLite3 (cho việc kiểm tra và phát triển công việc), MySQL, PostgreSQL.
</li>
</ul>
</div>
<ul>
<h2><a name="workflow_nova">3. Workflow Nova</a></h2>  
<br>
![image](/uploads/15e28cf9b2019a9541120c40654d310a/image.png)

<h3>1. Một số component tham gia vào quá trình khởi tạo và dự phòng cho máy ảo</h3>

- `Dashboard` (Horizon): cung cấp giao diện web cho việc quản trị các dịch vụ trong OpenStack 

- `CLI`: Command Line Interpreter - giao diện dòng lệnh để thực hiện các command gửi tới OpenStack Compute 

- `Compute(Nova)`: quản lý vòng đời máy ảo, từ lúc khởi tạo cho tới lúc ngừng hoạt động, tiếp nhận yêu cầu tạo máy ảo từ người dùng.   
- `Network - Quantum (hiện tại là Neutron)`: cung cấp kết nối mạng cho Compute, cho phép người dùng tạo ra mạng riêng của họ và kết nối các máy ảo vào mạng riêng đó.  
- `Block Storage (Cinder)`: Cung cấp khối lưu trữ bền vững (volume) cho các máy ảo  
- `Image(Glance)`: lưu trữ đĩa ảo trên Image Store  
- `Identity(Keystone)`: cung cấp dịch vụ xác thưc và ủy quyền cho toàn bộ các thành phần trong OpenStack. 

- `Message Queue(RabbitMQ)`: thực hiện việc giao tiếp giữa các thành phần trong OpenStack như Nova, Neutron, Cinder.

<h3>2. Request flow trong quá trình tạo máy ảo</h3>

- <b>Bước 1</b>: Từ `Dashboard` hoặc `CLI`, nhập thông tin chứng thực (ví dụ: user name và password) và thực hiện lời gọi `REST` tới `Keystone` để xác thực    
- <b>Bước 2</b>: `Keystone` xác thực thông tin người dùng và tạo ra một token xác thực gửi trở lại cho người dùng, mục đích là để xác thực trong các bản tin request tới các dịch vụ khác thông qua REST  
- <b>Bước 3</b>: `Dashboard` hoặc `CLI` sẽ chuyển yêu cầu tạo máy ảo mới thông qua thao tác "launch instance" trên openstack dashboard hoặc "nova-boot" trên CLI, các thao tác này thực hiện `REST API` request và gửi yêu cầu tới `nova-api`  
- <b>Bước 4</b>: `nova-api` nhận yêu cầu và hỏi lại `keystone` xem auth-token mang theo yêu cầu tạo máy ảo của người dùng có hợp lệ không, nếu có thì hỏi quyền hạn truy cập của người dùng đó.  
- <b>Bước 5</b>: `Keystone` xác nhận token và update lại trong header xác thực với roles và quyền hạn truy cập dịch vụ lại cho `nova-api`.   
- <b>Bước 6</b>: `nova-api` tương tác với `nova-database`
- <b>Bước 7</b>: `nova-database` tạo ra entry lưu thông tin máy ảo mới   
- <b>Bước 8</b>: `nova-api` gửi `rpc.call request` tới `nova-scheduler` để cập nhật entry của máy ảo mới với giá trị host ID (ID của máy compute mà máy ảo sẽ được triển khai trên đó). ( yêu cầu này lưu trong hàng đợi của Message Broker - RabbitMQ)   
- <b>Bước 9</b>: `nova-scheduler` lấy yêu cầu từ hàng đợi   
- <b>Bước 10</b>: `nova-scheduler` tương tác với nova-database để tìm host compute phù hợp thông qua việc sàng lọc theo cấu hình và yêu cầu cấu hình của máy ảo  
- <b>Bước 11</b>: `nova-database` cập nhật lại entry của máy ảo mới với host ID phù hợp sau khi lọc.  
- <b>Bước 12</b>: `nova-scheduler` gửi `rpc.cast request` tới `nova-compute`, mang theo yêu cầu tạo máy ảo mới với host phù hợp.( yêu cầu này lưu trong hàng đợi của Message Broker - RabbitMQ)  
- <b>Bước 13</b>: `nova-compute` lấy yêu cầu từ hàng đợi.   
- <b>Bước 14</b>: `nova-compute` gửi `rpc.call request` tới `nova-conductor` để lấy thông tin như host ID và flavor(thông tin về RAM, CPU, disk) (chú ý, nova-compute lấy các thông tin này từ database thông qua nova-conductor vì lý do bảo mật, tránh trường hợp nova-compute mang theo yêu cầu bất hợp lệ tới instance entry trong database)  
- <b>Bước 15</b>: `nova-conductor` lấy yêu cầu từ hàng đợi  
- <b>Bước 16</b>: `nova-conductor` tương tác với `nova-database`  
- <b>Bước 17</b>: `nova-database` trả lại thông tin của máy ảo mới cho nova-conductor, `nova condutor` gửi thông tin máy ảo vào hàng đợi.  
- <b>Bước 18</b>: `nova-compute` lấy thông tin máy ảo từ hàng đợi  
- <b>Bước 19</b>: `nova-compute` thực hiện lời gọi REST bằng việc gửi token xác thực tới `glance-api` để lấy Image URI với Image ID và upload image từ image storage.  
- <b>Bước 20</b>: `glance-api` xác thực auth-token với keystone  
- <b>Bước 21</b>: `nova-compute` lấy metadata của image(image type, size, etc.)  
- <b>Bước 22</b>: `nova-compute` thực hiện REST-call mang theo auth-token tới `network-api` để xin cấp phát IP và cấu hình mạng cho máy ảo  
- <b>Bước 23</b>: `neutron server` xác thực auth-token với keystone  
- <b>Bước 24</b>: `nova-compute` lấy thông tin về network  
- <b>Bước 25</b>: `nova-compute` thực hiện Rest call mang theo auth-token tới `volume_api` để yêu cầu volumes gắn vào máy ảo</li>
- <b>Bước 26</b>: `cinder-api` xác thực auth-token với `keystone`  
- <b>Bước 27</b>: `nova-compute` lấy thông tin block storage cấp cho máy ảo  
- <b>Bước 28</b>: `nova-compute` tạo ra dữ liệu cho `hypervisor driver` và thực thi yêu cầu tạo máy ảo trên Hypervisor (thông qua libvirt hoặc api - các thư viện tương tác với hypervisor)  
</ul>

<br> 
> Bảng biểu thị trạng thái `instance` ở các bước khác nhau trong quá trình cung cấp   

| **Status** | **Task** | **Power state** | **Steps** |  
| ------ | ------ |------ |------ |  
| Build | scheduling |None |3-12 |  
| Build | networking |None |22-24 |  
| Build | block_device_mapping |None |25-27 |  
| Build | spawing |None |28 |   
| Active | none |Running | |  

<h2><a name="ref">4. Tham khảo</a></h2>
<div>
[1] - <a href="https://ilearnstack.wordpress.com/2013/04/26/request-flow-for-provisioning-instance-in-openstack/">https://ilearnstack.wordpress.com/2013/04/26/request-flow-for-provisioning-instance-in-openstack/</a>
<br>
[2] - <a href="https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/11/html/architecture_guide/Components#comp-compute">https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/11/html/architecture_guide/Components#comp-compute</a>
<br>
[3] - <a href="https://docs.openstack.org//nova/latest/doc-nova.pdf">https://docs.openstack.org//nova/latest/doc-nova.pdf</a>
</div>

