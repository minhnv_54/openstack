### II.Tạo mới và thực hiện các action của 1 VM
#### 2.1 Tạo mới 1 VM 
- Step 1: `project` --> `Computer` --> `Instances` ->> `Launch Instance` , điền tên VM và 1 chút mô tả nếu có, chọn `Zone` VM thuộc về và số lượng VM muốn tạo chung 1 cấu hình như thế

![image](/uploads/6196f548848f4dad31f475e3b144b0e9/image.png) 
- Step 2: Tại phần `Source`, `Select Boot Source` có 4 option : `image` - build vm từ image, snapshot of an instance (`image snapshot`), `volume` or a `volume snapshot` (if enabled). Trong trường hợp này, em tạo VM qua image centos_7.6._final), lựa chọn mode tạo volume mới kèm theo với `volume size` = 40Gb  

![image](/uploads/15b71e16ab82471237e554c51c2bc329/image.png) 

- Step 3: chọn các thông số về Ram, vcpu, disk tại `flavor`, network tại `network` và click `Launch Instance` để hoàn thành việc tạo VM cơ bản 
![image](/uploads/b369a81b876ab25e5f20666dbd5bba53/image.png) 
![image](/uploads/0da70a28db1c99e25819f467bf1b12d9/image.png)) 
![Screenshot_from_2020-08-18_16-34-01](/uploads/47074aa4f796c499046e7978a735dd53/Screenshot_from_2020-08-18_16-34-01.png) 

#### 2.2 Các tác động liên quan đến VM   
![image](/uploads/17a08dc6dac89562d61c7c9ce0e8f4e8/image.png)  

>**Create snapshot**  
- Tạo ra 1 image lưu trữ trạng thái và dữ liệu của VM đang chạy hiện tại. 
 
![image](/uploads/3b834fdde8f013a546b87fb4b262db42/image.png)   

>**Associate Floating IP**  
- Khi VM được tạo trong Openstack, nó sẽ tự động được gán địa chỉ IP cố định trong mạng mà nó được chỉ định, địa chỉ tồn tại đến khi VM bị xóa, và dùng trong vùng mạng đó thôi
- Floating IP là địa chỉ nổi, nói chung là địa chỉ thứ 2 để truy cập vào VM từ vùng mạng ngoài, địa chỉ này có thể thay đổi lúc nào cũng được.  

>**Attach interface**  or **Detach interface** 
- Thêm/xóa interface cho VM   
![image](/uploads/d5a7940db65cdbb694a3638115bcdc42/image.png) 
![image](/uploads/50d7c703330bc7b6c0a14c11eca39c55/image.png)

>**Edit instance** or **Edit sercurity group**   

![image](/uploads/ddb5e1ca6c8ddd4dad99400941dd1abb/image.png) 

![image](/uploads/7ce8f6224d834bc0d688eb4f715d15c3/image.png)
- `Sercurity group` là add các rule network, sercurity group default:  
![image](/uploads/c6586111a31b639ede996fcada3b0560/image.png)   

>**Edit port sercurity group** 
- `Edit sercurity group` sẽ áp dụng cho tất cả interface, còn `edit port sercurity group` sẽ chỉ áp dụng cho port đó  
![image](/uploads/236e38e828739df8b3e233b3e9570877/image.png) 

>**Console** 
- Truy cập vào terminal VM    
![image](/uploads/0ae4ba9e7718cbc21c161280d5527deb/image.png)

>**Pause instance** and **suspend instance** and **shelve instance**
- Khi một VM bị tạm dừng, toàn bộ trạng thái của nó được giữ trong RAM. Tạm dừng một VM sẽ vô hiệu hóa quyền truy cập vào nó, nhưng sẽ không giải phóng bất kỳ tài nguyên nào. 

![image](/uploads/e669dbe299421322ac3427fa41af378d/image.png) 

- Giống `Pause`, VM bị treo sẽ giữ trạng thái hiện tại của nó, nhưng nó được ghi vào bộ nhớ. Khi bị treo sẽ giải phóng tài nguyên của VM, là lựa chọn tốt hơn cho các trường hợp mà tổ chức không cần trong một khoảng thời gian dài hơn.

![image](/uploads/cef92f4a54002b8ee85966e542186b5e/image.png)
-  Khi chuyển sang `shelve instance`, nó sẽ vẫn giữ được trạng thái của VM  nhưng không làm tiêu tốn tài nguyên, sẽ ở chế độ shutdown

![image](/uploads/1936967258e8b18cf689ea153316f24a/image.png)

>**Resize instance**  
- Thay đổi lại flavor của VM
![image](/uploads/682f6b3f604d64b3f8a27dd53c3c55bd/image.png) 

>**Lock instance** 

- Chặn hành đồng vào VM từ non-admin 

>**Soft reboot instance**  and **Hard reboot instance** 
- Soft reboot thì khởi động lại VM mà không tắt nguồn điện (chế độ mặc định khi thực hiện bằng dòng lệnh), hard reboot tắt và khởi động lại nguồn điện ( thêm --hard vào khi thực hiện bằng dòng lệnh)
> **Migrate, Live-migrate**   
- `Live-Migrate`: chuyển VM sang 1 host khác ngay cả khi VM đang chạy, nếu trong trường hợp host đó đang cao tải
- `Migrate`: tương tự như `live-migrate` nhưng trong quá trình migrate VM sẽ bị power off và rebiuld ở host đích    

> **Resize Instance**   

Thay đổi lại flavor của VM với `thông số cao hơn`

> **Pause, Subpend, Shutdown**
- `Pause` : toàn bộ trạng thái của VM được giữ trong RAM. Tạm dừng một VM sẽ vô hiệu hóa quyền truy cập vào nó, nhưng sẽ không giải phóng bất kỳ tài nguyên nào (giống như 'sleep'). Sử dụng `Pause` trong trường hợp muốn sao lưu, trước khi sao lưu một VM thì đặt nó vào trạng thái `Pause`  
- `Subpend`: Giống `Pause`, VM ở trạng này sẽ giữ trạng thái hiện tại của nó, nhưng toàn bộ trạng thái, dữ liệu sẽ được ghi vào disk. Trong trạng thái này, RAM và vCPU sẽ được giải phóng để sẵn sàng sử dụng cho VM mới khác, VM sẽ bị stop. Thông thường thì một VM lâu không dùng sẽ rơi vào trạng thái này để tiết kiệm tài nguyên máy chủ. Thường đặt chế độ này khi không muốn sử dụng VM trong 1 thời gian, hoặc khi muốn xóa VM (trước khi xóa VM thì nên đặt VM ở chế độ `suspend`, nếu có dịch vụ hoạt động sẽ đưa ra cảnh báo, còn nếu ko có mới tiến hành xóa VM để không làm ảnh hưởng đến dịch vụ đang chạy) 
- `Shutdown`: power off VM   

>**Tạo VM boot từ volume và image**  
- Từ `image`: khi dùng lệnh `lsblk` show dung lượng VM == `total disk của flavor` 
- Từ `volume`: khi dùng lệnh `lsblk` show dung lượng VM == `volume`   

>**Qcow2 & raw (type image)**  

- `QEMU` là `type 2 hypervisor` chạy trong không gian người dùng để thực hiện ảo hóa phần cứng. QEMU có thể tự chạy và mô phỏng tất cả tài nguyên của máy ảo, nhưng nó chạy trên phần mềm nên chậm. Còn `KVM` là `type 1 hypervisor` (`KVM is a Linux kernel module`)  chạy trong không gian kerel, nó chạy `full virtualization` (khi một CPU được mô phỏng (vCPU) bởi hypervisor, hypervisor phải dịch các lệnh dành cho vCPU sang CPU vật lý, mà không phải thực hiện trực tiếp trên CPU vật lý)nên tác động lớn đến hiệu suất =>> `The kernel component of KVM is included in mainline Linux. The userspace component of KVM is included in mainline QEMU`
![image](/uploads/0e05ce34b81b608c0debc05148598cb2/image.png)

- `QEMU` hỗ trợ nhiều định dạng `image`, có 2 loại chính là `qcow2` và `raw`: 
   - `raw`: Định dạng này có lợi thế là đơn giản và có thể dễ dàng xuất sang các trình giả lập khác. Ngoài ra hiệu suất của nó cao hơn qcow2 do không yêu cầu định dạng và hoạt động bổ sung đối với máy chủ trong hoạt động lưu trữ dữ liệu trên VM.  
   - `qcow2`: Định dạng linh hoạt nhất. Qcow2 có thể cung cấp imgae small (thin provisioning), mã hóa, nén và hỗ trợ multiple VM snapshots. Vì `qcow2 image` thường nhỏ hơn `RAW image`, do đó, việc chuyển đổi raw sang qcow2 để tải lên thường nhanh hơn thay vì boot trực tiếp từ raw. Ngoài ra vì raw không có tính năng snapshot nên khi cần Openstack sẽ tự động convert raw sang qcow2   

->> Khi lựa chọn loại nào cần cân nhắc giữa hiệu suất và tính năng 

>**Phân vùng (LVM)** 

- `LVM (Logical Volume Manager)` là một công cụ để quản lý phân vùng logic được tạo và phân bổ từ các ổ đĩa vật lý.   
- Mô hình LVM:  

![image](/uploads/a7bd33f898426c1292e2537f2b0342f8/image.png)  
    - `Physical Volumes`: Một ổ đĩa vật lý có thể phân chia thành nhiều phân vùng vật lý  gọi là Physical Volumes.    
    - `Volume Group`: Là một nhóm bao gồm nhiều `Physical Volume` trên 1 hoặc nhiều ổ đĩa khác nhau được kết hợp lại thành một `Volume Group`   
    - `Logical Volume`: Một `Volume Group` được chia nhỏ thành nhiều Logical Volume. Nó được dùng cho các để mount tới hệ thống tập tin (File System) và được format với những chuẩn định dạng khác nhau như ext3, ext4, xfs…   
    - `File Systems`: Hệ thống tập tin quản lý các file và thư mục trên ổ đĩa, được mount tới các `Logical Volume` trong mô hình LVM

> **Tạo volumn mới -> attach vào VM và tạo phân vùng -> mount & add data.** 
- Tạo volume mới, attach vào VM 
![image](/uploads/f5b031fa81d5fe28ea7bb961936f59f2/image.png)   
![image](/uploads/f9f46b43a80380ed15e2e6b1c80a149e/image.png) 
![image](/uploads/67aa1d844aa40fa34219d5e6944f7868/image.png)
- Từ mô hình LVM, phải tạo theo các bước từ `physical volumes` -> `Volume Group` -> `Logical Volume` -> `File Systems` 
- Step 1: Tạo **physical volume**:  `[root@lvm-is-da-best ~]# pvcreate /dev/vdc` 

![image](/uploads/74523f4acb5f90d932e95697dd4b0c79/image.png) 

   - Step 2: Tạo **Volume Group**: `[root@lvm-is-da-best ~]#vgcreate vg_u01 /dev/vdc`

![image](/uploads/15407e925c1f465a02cf24e3aaff087f/image.png) 
   - Step 3: Tạo **Logical Volume**: `[root@lvm-is-da-best ~]#lvcreate -n lv_u01 -l 100%FREE vg_u01`  
![image](/uploads/97f0f619f75356de5440475c40993b65/image.png)
- Step 4: Tạo **File Systems**:  `[root@lvm-is-da-best ~]#mkfs.ext4 /dev/vg_u01/lv_u01`  

![image](/uploads/2ebab1d79c2f9366ac6093f7ed1a57b8/image.png)
 - Step 5: 
`[root@lvm-is-da-best ~]# mkdir /u01`  
  
`[root@lvm-is-da-best ~]# mount /dev/vg_u01/lv_u01`   

![image](/uploads/1765841bdccbb69af03fdc2a68033514/image.png)




