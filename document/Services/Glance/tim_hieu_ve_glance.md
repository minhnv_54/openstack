# Openstack Image Service (Glance)

## **Mục lục**
[1.	Giới thiệu Glance](#glance1)

[2.	Các thành phần của Glance](#glance2)

[3.	Kiến trúc Glance](#glance3)

[4.	Glance image format](#glance4)

- [4.1.	Disk Formats](#glance4.1)

- [4.2.	Container Formats](#glance4.2)

[5.	Luồng trạng thái của image trong Glance](#glance5)

[6.	Tham khảo](#glance6)

---

<a name = "glance1"></a>
# 1. Giới thiệu Glance

-	`Glance` là **Image services** của Openstack cung cấp các chức năng: `discovering`, `registering`, `retrieving` for disk and server images.  
-  `Glance` là một trung tâm lưu trữ các virtual image.

-	Glance có RESTful API cho phép truy vấn vào VM image cũng như khôi phục lại các image hiện tại.

-	VM image được tạo sẵn, thông qua glance có thể được lưu trữ trong nhiều vị trí khác nhau từ các hệ thống tệp tin đơn giản đến các hệ thống lưu trữ đối tượng như là OpenStack Swift project.

-	Trong Glance, các images được lưu trữ giống như các template. Các template này sử dụng để vận hành máy ảo mới. Glance là giải pháp để quản lý các disk image trên cloud. 

-	Glance được thiết kế để có thể là dịch vụ hoạt động độc lập cho sự cần thiết các tổ chức lớn lưu trữ và quản lý các dish image ảo. 

-	Nó cũng có thể snapshots từ các máy ảo đang chạy để sao lưu trạng thái của VM.
-   `Xóa image sẽ không ảnh hưởng đến instance và snapshot của instance được boot từ image đó`

<a name = "glance2"></a>
# 2.	Các thành phần của Glance

-  Glance bao gồm các thành phần sau:

   -	**glance-api**: tiếp nhận lời gọi API để tìm kiếm, thu thập và lưu trữ image

   -	**glance-registry**: thực hiện tác vụ lưu trữ, xử lý và thu thập metadata của images

   -	**database**: cơ sở dữ liệu lưu trữ metadata của image

   -	**storage repository**: được tích hợp với nhiều thành phần khác trong OpenStack như hệ thống file thông thường, Amazon và HTTP phục vụ cho chức năng lưu trữ images

![image](/uploads/82f84934af6e93167e8a8421d1e1dab2/image.png)

- Glance tiếp nhận các API request yêu cầu images từ user hoặc các nova component và có thể lưu trữ các file images trong `object storage service`(`Swift`) hoặc các storage repos khác.

- Glance hỗ trợ các hệ thống back-end stores:
  -	***File system***: Glance lưu trữ images của các máy ảo trong hệ thống tệp tin thông thường theo mặc định, hỗ trợ đọc ghi các image files dễ dàng vào hệ thống tệp tin

  -	***Object Storage***: 
    
      - Là một service của Openstack- lưu trữ các image dưới dạng các object.

  -	***Block Storage***: 
    
      - Là một service của Openstack- lưu trữ các image dưới dạng các block

  -	***VMWare***

  -	***Amazon S3***

  -	***HTTP***

  -	***RADOS Block Device(RBD)*** : Lưu trữ các images trong cụm lưu trữ Ceph sử dụng giao diện RBD của Ceph

  -	***Sheepdog***: Hệ thống lưu trữ phân tán dành cho QEMU/KVM

  -	***GridFS***: Lưu trữ các image sử dụng MongoDB

<a name = "glance3"></a>
# 3.	Kiến trúc Glance

- `Glance` có kiến trúc **client-server** và ***cung cấp REST API*** thông qua đó yêu cầu tới server được thực hiện. 

- Yêu cầu từ `Client` được tiếp nhận thông qua REST API và đợi sự xác thực của Keystone. `Glance Domain Controller` quản lý tất cả các tác vụ vận hành bên trong. Các tác vụ này chia thành các lớp, mỗi lớp triển khai nhiệm vụ vụ riêng của chúng.

- `Glance Store Driver` là lớp giao tiếp giữa Glance và các hệ thống backend bên ngoài hoặc hệ thống tệp tin cục bộ, cung cấp giao diện chung để truy cập. Glance sử dụng SQL Database làm điểm truy cập cho các thành phần khác trong hệ thống.  
![image](/uploads/472a7b7bf719fc448ac7f8fa10a85544/image.png)
   
- Kiến trúc Glance bao gồm các thành phần sau: 

  -	***Client***: ứng dụng sử dụng Glance server.

  -	***REST API***: gửi yêu cầu tới Glance thông qua REST API.

  -	***Database Abstraction Layer (DAL)*** : là một API thống nhất việc giao tiếp giữa Glance và databases.

  -	***Glance Domain Controller***: là middleware triển khai các chức năng chính của Glance: ủy quyền, thông báo, các chính sách, kết nối cơ sở dữ liệu.

  -	***Glance Store***: tổ chức việc tương tác giữa Glance và các hệ thống lưu trữ dữ liệu.

  -	***Registry Layer***: lớp tùy chọn tổ chức việc giao tiếp một cách bảo mật giữa domain và DAL nhờ việc sử dụng một dịch vụ riêng biệt



<a name = "glance4"></a>
# 4.	Glance image format

Glance hỗ trợ nhiều loại định dạng đĩa (Disk format) và định dạng container (Container format). Virtual disk giống với các driver boot máy vật lý, chỉ khác là tất cả gộp lại trong một file. Các nền tảng ảo hóa khác nhau cũng hỗ trợ các định dạng disk format khác nhau.

<a name = "glance4.1"></a>
## 4.1.	Disk Formats  
![image](/uploads/4cf872e9126909b88718b0f042587bc4/image.png)


<a name = "glance4.2"></a>
## 4.2.	Container Formats  
Container format xác định loại và mức độ chi tiết của siêu dữ liệu máy ảo để lưu trữ trong image. 

![image](/uploads/430788a50556d7319397d4fa8e09245d/image.png)
 

<a name = "glance5"></a>
# 5.	Luồng trạng thái của image trong Glance

Luồng trạng thái của Glance cho biết trạng thái của image trong quá trình tải lên. Khi tạo một image, bước đầu tiên là `queuing`, `image` được đưa vào hàng đợi trong một khoảng thời gian ngắn, được bảo vệ và sẵn sàng để tải lên. Sau khi queuing, image chuyển sang trạng thái saving nghĩa là quá trình tải lên chưa hoàn thành. Một khi image được tải lên hoàn toàn, trạng thái image chuyển sang Active. Khi quá trình tải lên thất bại nó sẽ chuyển sang trạng thái bị hủy hoặc bị xóa. Ta có thể deactive và reactive các image đã upload thành công bằng cách sử dụng command. 
Luồng trạng thái của flow được mô tả theo hình sau:

![image](/uploads/c634b7f4b1ee36df4f60ea540186a09c/image.png)

Các trạng thái của image:

\- **queued**  

   Định danh của image được bảo vệ trong Glance registry. Không có dữ liệu nào của image được tải lên Glance và kích thước của image không được thiết lập rõ ràng sẽ được thiết lập về zero khi khởi tạo.

\- **saving**  
   Trạng thái này biểu thị rằng dữ liệu thô của image đang upload lên Glance. Khi image được đăng ký với lời gọi POST /images và có một header đại diện x-image-meta-location, image đó sẽ không bao giờ được đưa và trạng thái "saving" (bởi vì dữ liệu của image đã có sẵn ở một nơi nào đó)

\- **active**  

   Biểu thị rằng một image đã sẵn sàng trong Glance. Trạng thái này được thiết lập khi dữ liệu của image được tải lên hoàn toàn.

\- **deactivated**  

   Trạng thái biểu thị việc không được phép truy cập vào dữ liệu của image với tài khoản không phải admin. Khi image ở trạng thái này, ta không thể tải xuống cũng như export hay clone image.

\- **killed**  

   Trạng thái biểu thị rằng có vấn đề xảy ra trong quá trình tải dữ liệu của image lên và image đó không thể đọc được

\- **deleted**
  
   Trạng thái này biểu thị việc Glance vẫn giữ thông tin về image nhưng nó không còn sẵn sàng để sử dụng nữa. Image ở trạng thái này sẽ tự động bị gỡ bỏ vào ngày hôm sau.
   


<a name = "glance6"></a>
# 6.	Tham khảo

[1] http://www.sparkmycloud.com/blog/openstack-glance/

[2] https://docs.openstack.org/glance/latest/#user-guide



