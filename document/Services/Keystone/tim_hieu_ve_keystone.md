# Openstack Identify Service (Keystone)

## ***Mục lục***

[1. Giới thiệu](#keystone1)  

[2. Các chức năng chính của Keystone](#keystone2)

[3. Các thành phần của Keystone](#keystone3)



[4. Quá trình làm việc với token trong Openstack](#keystone4)

[5. Tham khảo](#5)

---

<a name = "keystone1"></a>
# 1. Giới thiệu

-    OpenStack Identity service cung cấp chức năng quản lý authentication, authorization và  catalog of services 
-   Identity service thường là dịch vụ đầu tiên mà người dùng tương tác. Sau khi được xác thực, người dùng cuối có thể sử dụng danh tính của họ (identity) để truy cập các dịch vụ OpenStack khác. Tương tự như vậy, các dịch vụ OpenStack khác tận dụng dịch vụ Identity để đảm bảo người dùng đúng như họ nói và discover vị trí của các dịch vụ khác trong quá trình triển khai. Identity service cũng có thể tích hợp với một số hệ thống quản lý người dùng bên ngoài (chẳng hạn như LDAP).

<a name = "keystone2"></a>  
# 2. Các chức năng chính của Keystone

- **Identity – Định danh**: 

  - Đề cập tới việc định danh cho người dùng đang truy cập vào tài nguyên của Openstack, định danh đơn giản được hiểu là đại diện cho một user. 


- **Authentication – Xác thực**: 

  - Là quá trình xử lý sự hợp lệ của định danh user. 

  - Trong nhiều trường hợp, sự xác thực là việc thực hiện đầu tiên cho user bằng cách login vào hệ thống với user identity và mật khấu. 

- **Access Management (Authorization)**:

  -  Một user identity đã được xác thực và được tạo, cấp phát token, sẵn bắt đầu Access management. Access Management cũng như là xác thực, là quá trình kiểm tra xem những tài nguyên nào user được phép truy cập. 

  - Ví dụ, cần phải có một cơ chế để kiểm tra xem những user nào được phép tạo những loại máy ảo đặc biệt, người dùng nào được phép gán hoặc xóa volumn trong khối lưu trữ, người dùng nào được phép tạo mạng ảo, …. 

  - Trong OPS, Keystone maps ***User*** tới ***Project*** hoặc ***Domain*** bằng cách liên kết ***Role*** cho người dùng tới các Project và Domain đó. Các project khác trong OPS như Nova, Cinder và Neutron kiểm tra Project của User và xem Role được gán cùng, đánh giá cá thông tin này sử dụng một cơ chế chính sách.. Cơ chế này sẽ kiểm tra những thông tin này và quyết định về những hành động mà user được phép thực hiện .


- **Một số chức năng khác**: Trong khi Keystone hầu hết tập trung và việc cung cấp khả năng định danh, xác thực và ủy quyền, nó còn cung cấp nhiều lợi ích khác cho OPS như:

  -	***Cung cấp sự xác thực và ủy quyền cho tất cả các dịch vụ khác của OPS***: Keystone giải quyết vấn đề phức tạp trong việc kết hợp xác thực hệ thống external và cũng cung cấp sự xác thực giống nhau cho tất cả các dịch vụ khác của OPS như Nova, Glance, Cinder, Neutron, … và do đó cô lập tất cả các dịch vụ từ việc biết cách làm thế nào để phân biệt định danh và xác thực giữa các dịch vụ.

  -	***Cung cấp sự đăng ký cho các container (Project)*** để các dịch vụ khác của OPS cần sử dụng để phân chia tài nguyên.

  -	***Cung cấp việc đăng ký các domain*** được sử dụng để định nghĩa phân chia namespace cho các user, group và các project để cho phép phân chia giữa các khách hàng.

  -	***Tạo các role*** sẽ được sử dụng để xác thực giữa Keystone và các file chính sách trong mỗi dịch vụ của OPS.

  -	***Tạo sự kết nối*** cho phép user và group được kết nối với các role trên project và domain.

  -	***Catalog - mục lục*** lưu trữ các các dịch vụ, endpoints, và regions của OPS, cho phép các clients tìm ra dịch vụ hoặc endpoint mà họ cần. 

<a name = "keystone3"></a>  
# 3. Các thành phần của Keystone  

> "Since the Rocky release Keystone uses the `Flask-RESTful` library to provide a REST API interface to these services." 

- **Identity**
   -    Identity service cung cấp xác thực thông tin xác thực và dữ liệu về `user` và `group`. Trong trường hợp cơ bản, dữ liệu này được quản lý bởi Identity service, cho phép nó cũng xử lý tất cả các hoạt động CRUD liên quan đến dữ liệu này. Trong những trường hợp phức tạp hơn, dữ liệu được quản lý bởi một `authoritative backend
service` ( ex: LDAP)

- **Project**, **Groups**, **User**  
  - Projects đại diện cho đơn vị sở hữu cơ bản trong openstack, trong đó tất cả resource thuộc về 1 project cụ thể.
  - Groups là một vùng đại diện cho một tập hợp các user.

  - User đại diện cho một người dùng API cá nhân. 
  - Bản thân project, groups, user phải nằm trong 1 domain cụ thể  -> Nó không phải là duy nhất trên toàn cầu mà chỉ duy nhất trong domain mà nó thuộc về.  
![image](/uploads/009c81e95c3c71d461a6ef04564c0b71/image.png)

- **Domains**
   -    Một Domain được xác định là một tập hợp các user, groups và các project.

- **Roles**
   -    Role chỉ định mức độ `authorization` của user, có cấp độ domain or project, được gán cho một user hoặc nhóm.

- **Assignment**  
   -    Assignment cung cấp data về `role` và `role assignment`
   -    `Role Assignments` gồm 3 thành phần: role, resource và identity

- **Token**
   -    Token service xác thực và quản lý token được sử dụng để xác thực yêu cầu sau khi thông tin xác thực của người dùng đã được xác minh 
   -    Mỗi token đều có một ID và payload. ID của token được đảm bảo là duy nhất trên mỗi cloud, và payload chứa dữ liệu về user.

- **Catalog**
   -    Dịch vụ catalog là cần thiết cho cloud OPS. Nó chứa các URL và các enpoint của các dịch vụ OPS khác nhau.
    - Cho phép các API client tự động khám phá và điều hướng đến các dịch vụ đám mây  

![image](/uploads/ae4a98d7c1f9c1e2c2f54c477bf9e48a/image.png)
<a name = "keystone4"></a>
# 4. Quá trình làm việc với token trong Openstack  

![image](/uploads/093320cc5d6245b7af0ec1146972922a/image.png)  
![image](/uploads/31cd8a34c854992a7d08bd5464b26e55/image.png)



<a name = "5"></a>
# 5. Tham khảo  

[1] https://docs.openstack.org//keystone/latest/doc-keystone.pdf   

[2] https://docs.openstack.org/ocata/install-guide-ubuntu/common/get-started-identity.html# 



