
#### Set up

1. Docker up một container chạy rabbitmq

docker run -d -e RABBITMQ_NODENAME=rabbitmq1 --name luonvuituoi_rbmq -p 5672:5672 -p 15672:15672 rabbitmq:3.5.3-management  

2. RabbitMQ libraries  
```
intern@intern-TravelMate-P246-M: python3 -m venv rabbitmq
intern@intern-TravelMate-P246-M: source rabbitmq/bin/ctivate
(rabbitmq) intern@intern-TravelMate-P246-M:pip install pika
```

3. Check ip container: 
```
(rabbitmq) intern@intern-TravelMate-P246-M:~/minhnv54/minhnv98-training/rabbitmq/tutorial$ docker inspect luonvuituoi_rbmq | grep IPAddress
            "SecondaryIPAddresses": null,
            "IPAddress": "172.17.0.2",
                    "IPAddress": "172.17.0.2",
```

#### I. Hello Word

* Sending 
Our first program `send.py` will send a single message to the queue. The first thing we need to do is to establish a connection with RabbitMQ server.  

```
import pika

connection = pika.BlockingConnection(pika.ConnectionParameters('172.17.0.2'))
channel = connection.channel()
```  
Tạo 1 `queu` tên là "hello", để message gửi vào đây: 
`channel.queue_declare(queue='hello')`  

Tạo 1 message chứa nội dung là "Hello World" đẩy vào `queu` vừa tạo: 
```
channel.basic_publish(exchange='',
                      routing_key='hello',
                      body='Hello World!')
print(" [x] Sent 'Hello World!'")
``` 

Result: 
```
intern@intern-TravelMate-P246-M:~/minhnv54/$ python send.py 
[x] Sent 'Hello World!'
intern@intern-TravelMate-P246-M:~/minhnv54/$ docker exec -it luonvuituoi_rbmq bash 
root@252c77c0107e:/# rabbitmqctl list_queues
Listing queues ...
hello	1
```
CR_NORMAL_IT_20022406524
Hi chị, em có 1 CR nâng RAM thực hiện ngày ạ, bên cơ điện tạo CR cấp 2 rùi ạ, chị sắp lịch thực thi giúp em từ 14h30 - 23h 12/1/2021 giúp em với ạ, Mã CR: CR_NORMAL_IT_20022406524 ạ

* Receving  

`receive.py` will receive messages from the queue and print them on the screen.
Connect tới RabbitMQ server, rạo ra `queu` tương ứng như queu `hello` bên gửi 
```
import pika

connection = pika.BlockingConnection(pika.ConnectionParameters('172.17.0.2'))
channel = connection.channel()
channel.queue_declare(queue='hello')
```  

Khi nhận được message, hàm `callback` sẽ được gọi, khai báo hàm như dưới trong trường hợp chỉ muốn print như dưới
```
def callback(ch, method, properties, body):
    print(" [x] Received %r" % body)
```  
Sau đó, cần cho RabbitMQ biết hàm callback sẽ được sử dụng ở queu `hello`
Tiếp theo, chúng ta cần nói với RabbitMQ rằng hàm gọi lại cụ thể này sẽ nhận được thông báo từ hàng đợi hello của chúng ta:
```
channel.basic_consume(queue='hello',
                      auto_ack=True,
                      on_message_callback=callback)
``` 
Cuối cùng tạo 1 vòng lặp vô hạn để chờ nếu có dữ liệu:
```
print(' [*] Waiting for messages. To exit press CTRL+C')
channel.start_consuming()
```  
> source code:  

Test: 
```
(rabbitmq) intern@intern-TravelMate-P246-M:~/minhnv54/minhnv98-training/rabbitmq/tutorial$ python receive.py 
 [*] Waiting for messages. To exit press CTRL+C
 [x] Received b'Hello World!'
 ```
 Khi consumer nhận được tin nhắn từ queu `hello` thì tin nhắn trong queu sẽ bị xóa đi 
 ```
 root@252c77c0107e:/# rabbitmqctl list_queues
Listing queues ...
hello	0
``` 
Thử chạy `send.py` để đẩy 1 message vào queu 'hello'



#### II. Work Queues   
Trong style này, sẽ có `nhiều consumer` chọc vào `1 queu` để lấy message. Ý tưởng chính đằng sau là tránh thực hiện một nhiệm vụ tiêu tốn nhiều resource ngay lập tức và phải đợi nó hoàn thành. Thay vào đó, ta lên lịch cho nhiệm vụ sẽ được thực hiện sau, đóng gói nhiệm vụ dưới dạng một message và gửi nó vào `queu`. Consumer sẽ lấy message thực thi công việc. Khi có nhiều consumer, các message sẽ được chia sẻ giữa họ.

- Ví dụ sau mô tả qua các làm việc của style này, file `new_task.py` coi như 1 `producer`, gửi các message đến rabbitmq, 
``` 
message = ' '.join(sys.argv[1:]) or "Hello World!"
channel.basic_publish(exchange='',
                      routing_key='hello',
                      body=message)
print(" [x] Sent %r" % message)
```
nội dung gói tin không phải "Hello word" cố định như phần I, mà sẽ được gửi từ command line, và cuối nội dung sẽ có `n dấu '.'` thể hiện thời gian xử lý gói tin đó. Ví dụ `python new_task.py "hello..."` thì consumer nhận gọi tin này sẽ xử lý mất 3s.
File `receive.py`- bên consumer sẽ nhận gọi tin, xử lý gọi tin với thời gian dựa vào số lượng dấu chấm trong nội dung tin nhắn bên gửi dựa vào function time.sleep()

```
def callback(ch, method, properties, body):
    print(" [x] Received %r" % body.decode())
    time.sleep(body.count(b'.'))
    print(" [x] Done")
```
Tạo ra 3 shell, 2 shell chạy file `receive.py` - đóng vai trò như consumer - C1 và C2,  

``` 
# shell 1
python receive.py
# => [*] Waiting for messages. To exit press CTRL+C 
# shell 2
python receive.py
# => [*] Waiting for messages. To exit press CTRL+C
``` 
shell 3 chạy file `new_task.py` - đóng vai trò `producer` đẩy 1 vài message vào rabbitmq: 
```
python new_task.py First message.
python new_task.py Second message..
python new_task.py Third message...
python new_task.py Fourth message....
python new_task.py Fifth message.....
```  
Lúc này trên 2 shell consumer sẽ nhận  message từ rabbitmq phân phối, rabbitmq mặc đinh dùng round robin để phân phối message - message lẻ cho consumer C1, message chẵn cho consumer C2

```
# shell 1
python reveive.py
# => [*] Waiting for messages. To exit press CTRL+C
# => [x] Received 'First message.'
# => [x] Received 'Third message...'
# => [x] Received 'Fifth message.....'
``` 
```
# shell 2
python reveive.py
# => [*] Waiting for messages. To exit press CTRL+C
# => [x] Received 'Second message..'
# => [x] Received 'Fourth message....'
```
Để tránh trường hợp consumer đang xử lý message thì bị sự cố dẫn đến mất message đang thực hiện dở, RabbitMQ hỗ trợ  message acknowledgments . Một `ack` (nowledgement) được consumer gửi lại để cho RabbitMQ biết massage đã được nhận và thực thi, RabbitMQ có quyền xóa nó. Nếu consumer die mà không gửi ack, RabbitMQ sẽ hiểu rằng một message không được xử lý thành công và sẽ xếp hàng lại, phân phối cho consumer khác. 
```
ch.basic_ack(delivery_tag = method.delivery_tag)
``` 

- Vì mặc định rabbit dùng thuật toán round robin để phân phối, nên sẽ xảy ra trường hợp có consumer hoàn thành xong công việc được giao sớm, ngồi chơi trong khi con consumer còn lại vẫn còn nhiều message nó được phân công chưa xử lý xong.
 Để tránh trường hợp này,  có thể sử dụng `Channel#basic_qos` với cài đặt `prefetch_count = 1`.Nó sử dụng method `basic.qos` để yêu cầu RabbitMQ không gửi nhiều hơn một message cho một consumer cùng một lúc. Hay nói cách khác, không gửi một message mới tới một consumer cho đến khi consumer đó xử lý và ack tin nhắn trước đó. Thay vào đó, nó sẽ gửi message đó cho consumer tiếp theo mà vẫn chưa bận


