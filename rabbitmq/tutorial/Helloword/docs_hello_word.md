#### I. Hello Word

* Sending 
Our first program `send.py` will send a single message to the queue. The first thing we need to do is to establish a connection with RabbitMQ server.  

```
import pika

connection = pika.BlockingConnection(pika.ConnectionParameters('172.17.0.2'))
channel = connection.channel()
```  
Tạo 1 `queu` tên là "hello", để message gửi vào đây: 
`channel.queue_declare(queue='hello')`  

Tạo 1 message chứa nội dung là "Hello World" đẩy vào `queu` vừa tạo: 
```
channel.basic_publish(exchange='',
                      routing_key='hello',
                      body='Hello World!')
print(" [x] Sent 'Hello World!'")
``` 

Result: 
```
intern@intern-TravelMate-P246-M:~/minhnv54/$ python send.py 
[x] Sent 'Hello World!'
intern@intern-TravelMate-P246-M:~/minhnv54/$ docker exec -it luonvuituoi_rbmq bash 
root@252c77c0107e:/# rabbitmqctl list_queues
Listing queues ...
hello	1
```


* Receving  

`receive.py` will receive messages from the queue and print them on the screen.
Connect tới RabbitMQ server, rạo ra `queu` tương ứng như queu `hello` bên gửi 
```
import pika

connection = pika.BlockingConnection(pika.ConnectionParameters('172.17.0.2'))
channel = connection.channel()
channel.queue_declare(queue='hello')
```  

Khi nhận được message, hàm `callback` sẽ được gọi, khai báo hàm như dưới trong trường hợp chỉ muốn print như dưới
```
def callback(ch, method, properties, body):
    print(" [x] Received %r" % body)
```  
Sau đó, cần cho RabbitMQ biết hàm callback sẽ được sử dụng ở queu `hello`
Tiếp theo, chúng ta cần nói với RabbitMQ rằng hàm gọi lại cụ thể này sẽ nhận được thông báo từ hàng đợi hello của chúng ta:
```
channel.basic_consume(queue='hello',
                      auto_ack=True,
                      on_message_callback=callback)
``` 
Cuối cùng tạo 1 vòng lặp vô hạn để chờ nếu có dữ liệu:
```
print(' [*] Waiting for messages. To exit press CTRL+C')
channel.start_consuming()
```  
> source code:  

Test: 
```
(rabbitmq) intern@intern-TravelMate-P246-M:~/minhnv54/minhnv98-training/rabbitmq/tutorial$ python receive.py 
 [*] Waiting for messages. To exit press CTRL+C
 [x] Received b'Hello World!'
 ```
 Khi consumer nhận được tin nhắn từ queu `hello` thì tin nhắn trong queu sẽ bị xóa đi 
 ```
 root@252c77c0107e:/# rabbitmqctl list_queues
Listing queues ...
hello	0
``` 
Thử chạy `send.py` để đẩy 1 message vào queu 'hello'
