Ta sẽ làm cho bạn chỉ có thể subscribe vào một tập hợp con của các messages, như chỉ có thể gửi trực tiếp các error messages đến log file, trong khi vẫn có thể print tất cả các thông báo log messages trên console.  
Phần trước mình đã tạo ra 1 binding kiểu như dưới: 
```
channel.queue_bind(exchange=exchange_name,
                   queue=queue_name)
``` 
Binding là 1 mối quan hệ giữa exchange và queue, queue quan tâm đến  messages từ exchange này, và binding định nghĩa cách phân phối messages từ exchange vào queue  

Binding có 1 tham số khác là `routing_key` - Tùy từng kiểu của exchange mà ý nghĩa nó sẽ khác nhau, với `fanout` exchanges, tham số này sẽ bị ignored. Cách để định nghĩa:
```
channel.queue_bind(exchange=exchange_name,
                   queue=queue_name,
                   routing_key='black')
``` 
Để ko bị nhầm lẫn với routing_key khai báo trong `basic_publish`, mk gọi routing_key này với tên gọi khác là `binding key`

#### Direct exchange  
Ở loại này, một message đi đến queue có `binding key` khớp  với `routing key` của message mới nhận được message này.  

![image](/uploads/1c38a47d11c58f30926746e4d71fa219/image.png)  
Ví dụ trên thì `bind key` `orange` sẽ vào queue Q1, trong khi bind key `black` và `green` sẽ vào queue Q2     

**LAB** 

![image](/uploads/17f5512243b2d59bee3c4e44f2a01ea7/image.png) 

`send.py`  


```
import pika
import sys

connection = pika.BlockingConnection(
    pika.ConnectionParameters(host='172.17.0.2'))
channel = connection.channel()

channel.exchange_declare(exchange='direct_logs', exchange_type='direct')

severity = sys.argv[1] if len(sys.argv) > 1 else 'info'
message = ' '.join(sys.argv[2:]) or 'Hello World!'
channel.basic_publish(
    exchange='direct_logs', routing_key=severity, body=message)
print(" [x] Sent %r:%r" % (severity, message))
connection.close()
``` 

`receive.py`  

```
import pika
import sys

connection = pika.BlockingConnection(
    pika.ConnectionParameters(host='172.17.0.2'))
channel = connection.channel()

channel.exchange_declare(exchange='direct_logs', exchange_type='direct')

result = channel.queue_declare(queue='', exclusive=True)
queue_name = result.method.queue

severities = sys.argv[1:]
if not severities:
    sys.stderr.write("Usage: %s [info] [warning] [error]\n" % sys.argv[0])
    sys.exit(1)

for severity in severities:
    channel.queue_bind(
        exchange='direct_logs', queue=queue_name, routing_key=severity)

print(' [*] Waiting for logs. To exit press CTRL+C')


def callback(ch, method, properties, body):
    print(" [x] %r:%r" % (method.routing_key, body))


channel.basic_consume(
    queue=queue_name, on_message_callback=callback, auto_ack=True)

channel.start_consuming()
```
Test: 
Trên terminal 1: 
`python3 receive.py error`  - chỉ nhận các gói tin error
Trên terminal 2: 
`python3 receive.py info warning`  - chỉ nhận các gói tin info warning 
Trên terminal 1: 
`python3 send.py error toang `  - gửi gói tin error với nội dung toang

